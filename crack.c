#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=50;        // Maximum any password will be


// Stucture to hold both a plaintext password and a hash.
typedef struct entry {
    char password[PASS_LEN + 1];
    char hash[33];
} entry;

int compare(const void *x, const void *y);
int compareB(const void *x, const void *y);

// Reads in the dictionary file and return an array of entry structs.
entry *read_dictionary(char *filename, int *size)
{
    *size = 0;
    FILE *fp = fopen(filename, "r");
    if (!fp) {
        perror("Unable to open file");
        exit(1);
    }

    // declerations
    int arraySize = 100;
    entry *array = malloc(sizeof(entry) * arraySize);
    char str[PASS_LEN + 2];

    while (fgets(str, PASS_LEN + 2, fp)) {

        // remove newline char
        char *newLine = strchr(str, '\n');
        if (newLine) {
            *newLine = '\0';
        }
        //hashes 
        char *hash = md5(str, strlen(str));
        if (*size < arraySize) {
            strcpy(array[*size].password, str);
            strcpy(array[*size].hash, hash);
        } else {
            arraySize += 100;
            array = realloc(array, arraySize * sizeof(entry));
            strcpy(array[*size].password, str);
            strcpy(array[*size].hash, hash);
        }
        free(hash);
        (*size)++;
    }
    fclose(fp);
    return array;
}

int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    int size;
    printf("loading dictionary\n");
    entry *dictionary = read_dictionary(argv[2], &size);
    
    printf("sorting\n");
    qsort(dictionary, size, sizeof(entry), compare);
    
    FILE *hashes = fopen(argv[1], "r");
    if (!hashes) {
        printf("Unable to open %s", argv[1]);
        exit(1);
    }
    char str[34];
    int num = 1;
    while (fgets(str, 34, hashes)) {
        char *newLine = strchr(str, '\n');
        if (newLine) {
            *newLine = '\0';
        }
        entry *found = bsearch(str, dictionary, size, sizeof(entry), compareB);

        if (found) {
            printf("hash #%d: %s password: %s\n", num, found[0].hash, found[0].password);
            num++;
        }
    }

    free(dictionary);
    fclose(hashes);
}

int compareB(const void *x, const void *y) {
    return strcmp(x, (*(entry*)y).hash);
}

int compare(const void *x, const void *y) {
     return strcmp(((entry*)x) -> hash, ((entry*)y) -> hash);
}

